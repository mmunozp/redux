// Archivo global que tiene toda la definición del estado

import * as fromUI from './shared/ui.reducer';
import { ActionReducerMap } from '@ngrx/store';

import * as fromAuth from './auth/auth.reducer';

// import * as fromIngresoEgreso from './ingreso-egreso/ingreso-egreso.reducer';

export interface AppState {
    ui: fromUI.State;
    auth: fromAuth.AuthState;
    // ingresoEgreso: fromIngresoEgreso.IngresoEgresoState;
}

export const appReducers: ActionReducerMap<AppState> = {    // Actionreducermap permite fusionar varios reducers en uno solo
    ui: fromUI.uiReducer,
    auth: fromAuth.authReducer,
    // ingresoEgreso: fromIngresoEgreso.ingresoEgresoReducer
};
