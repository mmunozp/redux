

export class User {
    public nombre: string;
    public email: string;
    public uid: string;

    constructor(data: DataObj) {
        this.nombre = data && data.nombre || null; // Si existe data, usa data.name. Si no, usa null.
        this.email = data && data.email || null;
        this.uid = data && data.uid || null;
    }
}

interface DataObj {
    uid: string;
    email: string;
    nombre: string;
}
