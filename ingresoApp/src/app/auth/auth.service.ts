import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import * as firebase from 'firebase';

import { map } from 'rxjs/operators';
import { User } from './user.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { ActivarLoadingAction, DesactivarLoadingAction } from '../shared/ui.acctions';
import { SetUserAction, UnsetUserAction } from './auth.actions';
import { Subscription } from 'rxjs';
import { UnsetItemsAction } from '../ingreso-egreso/ingreso-egreso.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private subscription: Subscription = new Subscription();
  private user: User;

  constructor( public afAuth: AngularFireAuth, private router: Router, private afDB: AngularFirestore, private store: Store<AppState> ) { }

  initAuthListener() {

    this.afAuth.authState.subscribe( (fbUser: firebase.User) => {
      if ( fbUser ) {
        this.subscription = this.afDB.doc(`${fbUser.uid}/usuario`).valueChanges().subscribe(
          (usuarioObj: any) => {
            const newUser = new User(usuarioObj);
            console.log(newUser);
            this.store.dispatch(new SetUserAction(newUser));
            this.user = newUser;
          }
        );
      } else {
        this.user = null;
        this.subscription.unsubscribe();
      }
    } );

  }

  crearUsuario(email: string, nombre: string, password: string) {

    this.store.dispatch(new ActivarLoadingAction()); // set loading state

    this.afAuth.auth
    .createUserWithEmailAndPassword(email, password)
    .then( resp => {
      const user: User = {
        uid: resp.user.uid,
        nombre,
        email: resp.user.email
      };
      this.afDB.doc(`${user.uid}/usuario`).set( // Crea una coleccion con uid como nombre e info del usuario en /usuario
        user
      ).then(() => {
        this.router.navigate(['/']);
        this.store.dispatch(new DesactivarLoadingAction()); // stop loading state
      }).catch(
        error => console.log(error)
      );


    }

    ).catch( error => {
      Swal.fire('Error en el login', error.message, 'error');
      this.store.dispatch(new DesactivarLoadingAction()); // stop loading state
    }

    );
  }

  login(email: string, password: string) {
    this.store.dispatch(new ActivarLoadingAction()); // Set isLoading state to true
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then( resp => {
      this.router.navigate(['/']); // Navigate to homepage
      this.store.dispatch(new DesactivarLoadingAction()); // Set isLoading state to false
    }).catch( error => { // If an error is thrown
      Swal.fire('Error en el login', error.message, 'error'); // Show Swal error popup
      this.store.dispatch(new DesactivarLoadingAction()); // Set isLoading state to false
    });
  }

  logout() {
    this.router.navigate(['/login']);
    this.afAuth.auth.signOut();
    this.store.dispatch( new UnsetUserAction() );

  }

  isAuth() {
    return this.afAuth.authState.pipe(
      map( fbUser => {
        if (fbUser === null) {
          this.router.navigate(['/login']);
        }
        return fbUser != null;
      } )
    );
  }

  getUser() {
    return { ... this.user };
  }

}
