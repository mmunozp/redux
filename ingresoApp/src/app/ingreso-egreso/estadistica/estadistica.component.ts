import { Component, OnInit } from '@angular/core';
import { IngresoEgreso } from '../ingreso-egreso.model';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Label, MultiDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';

import * as fromIngresoEgresoReducer from '../ingreso-egreso.reducer';

@Component({
  selector: 'app-estadistica',
  templateUrl: './estadistica.component.html',
  styles: []
})
export class EstadisticaComponent implements OnInit {

  ingresos: number;
  esgresos: number;

  ingresosMonto: number;
  esgresosMonto: number;

  subscription: Subscription = new Subscription();

  // Charts
  doughnutChartLabels: Label[] = ['Ingresos', 'Esgresos'];
  doughnutChartData: MultiDataSet;
  doughnutChartType: ChartType = 'doughnut';

  constructor(private store: Store<fromIngresoEgresoReducer.AppState>) { }

  ngOnInit() {
    this.subscription = this.store.select('IngresoEgreso')
    .pipe(filter(ingresoEgreso => ingresoEgreso != null)) // Only subscribe when data is recieved
    .subscribe( ingresoEgreso =>
      this.contarIngresosEsgresos(ingresoEgreso.items) );
  }

  contarIngresosEsgresos(items: IngresoEgreso[]) {
    this.ingresos = 0;
    this.esgresos = 0;
    this.ingresosMonto = 0;
    this.esgresosMonto = 0;


    items.forEach(item => {
      if (item.type === 'ingreso') {
        this.ingresos += item.quantity;
        this.ingresosMonto++;
      } else {
        this.esgresos += item.quantity;
        this.esgresosMonto++;
      }
    });

    this.doughnutChartData = [
      [this.ingresos, this.esgresos],
      // [this.ingresosMonto, this.esgresosMonto],
    ];
  }
}
