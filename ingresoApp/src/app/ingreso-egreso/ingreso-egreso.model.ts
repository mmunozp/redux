

export class IngresoEgreso {
    description: string;
    quantity: number;
    type: string;
    uid?: string;

  constructor(data: DataObj) {
    this.description = data && data.description || null; // Si existe data, usa data.name. Si no, usa null.
    this.quantity = data && data.quantity || null;
    this.type = data && data.type || null;
    this.uid = data && data.uid || null;
    }
}

interface DataObj {
    description: string;
    quantity: number;
    type: string;
    uid: string;
}
