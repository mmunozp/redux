import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import { filter } from 'rxjs/operators';
import { IngresoEgreso } from '../ingreso-egreso.model';
import { Subscription } from 'rxjs';
import { IngresoEgresoService } from '../ingreso-egreso.service';
import Swal from 'sweetalert2';

import * as fromIngresoEgresoReducer from '../ingreso-egreso.reducer';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styles: []
})
export class DetalleComponent implements OnInit, OnDestroy {
  subscription: Subscription = new Subscription();
  items: IngresoEgreso[];

  constructor(private store: Store<fromIngresoEgresoReducer.AppState>, private ingresoEgresoService: IngresoEgresoService) { }

  ngOnInit() {
    this.subscription = this.store.select('IngresoEgreso')
    .pipe(filter(ingresoEgreso => ingresoEgreso != null))
    .subscribe( ingresoEgreso => {
      this.items = ingresoEgreso.items;
    }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  deleteIngresoEgreso(item: IngresoEgreso) {
    this.ingresoEgresoService.deleteIngresoEgreso(item.uid).then( () => {
      Swal.fire('Borrado con exito', item.description , 'success');
    } );
  }

}
