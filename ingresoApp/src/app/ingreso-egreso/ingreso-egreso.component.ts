import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IngresoEgreso } from './ingreso-egreso.model';
import { IngresoEgresoService } from './ingreso-egreso.service';
import Swal from 'sweetalert2';
import { AppState } from '../app.reducer';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { ActivarLoadingAction, DesactivarLoadingAction } from '../shared/ui.acctions';
import * as fromIngresoEgresoReducer from './ingreso-egreso.reducer';


@Component({
  selector: 'app-ingreso-egreso',
  templateUrl: './ingreso-egreso.component.html',
  styles: []
})
export class IngresoEgresoComponent implements OnInit, OnDestroy {

  form: FormGroup;
  type = 'ingreso';
  loading: boolean;
  subscription: Subscription;

  constructor(private ingresoEgresoService: IngresoEgresoService, private store: Store<fromIngresoEgresoReducer.AppState>) { }

  ngOnInit() {
    this.subscription = this.store.select('ui').subscribe( ui => this.loading = ui.isLoading );

    this.form = new FormGroup({
    description: new FormControl('', Validators.required),
    quantity: new FormControl(0, Validators.min(0))
  });

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  crearIngresoEgreso() {
    const ingresoEgreso = new IngresoEgreso({ ... this.form.value, type: this.type });
    this.store.dispatch(new ActivarLoadingAction());
    this.ingresoEgresoService.createIngresoEgreso(ingresoEgreso).then(
      () => {
        Swal.fire('Transcacción realizada',
         `Su transacción de tipo <b> ${this.type} </b> por importe de <b> ${this.form.value.quantity} </b>
         con descripción <b> ${this.form.value.description} </b> ha sido realizada con exito.`,
          'success');

        this.form.reset({
          description: '',
          quantity: 0
        });
        this.store.dispatch(new DesactivarLoadingAction());
      }
    );
  }

}
