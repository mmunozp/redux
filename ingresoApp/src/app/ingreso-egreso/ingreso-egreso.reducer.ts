import * as fromIngresoEgreso from './ingreso-egreso.actions';
import { IngresoEgreso } from './ingreso-egreso.model';
import { AppState } from '../app.reducer';

export interface IngresoEgresoState {
    items: IngresoEgreso[];
}

export interface AppState extends AppState {
  IngresoEgreso: IngresoEgresoState;
}

const initState: IngresoEgresoState = {
    items: []
};

export function ingresoEgresoReducer(state: IngresoEgresoState, action: fromIngresoEgreso.acciones) {
    switch (action.type) {
        case fromIngresoEgreso.SET_ITEMS:
            return {
                items: [ ... action.items.map( item => ({ ... item }) ) ]
            };
        case fromIngresoEgreso.UNSET_ITEMS:
            return {
                items: []
            };

        default:
            return state;

    }
}

