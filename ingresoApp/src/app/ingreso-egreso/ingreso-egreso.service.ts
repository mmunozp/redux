import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { IngresoEgreso } from './ingreso-egreso.model';
import { AuthService } from '../auth/auth.service';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { filter, map } from 'rxjs/operators';
import { SetItemsAction, UnsetItemsAction } from './ingreso-egreso.actions';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class IngresoEgresoService {

  ingresoEgresoItemsSubscription: Subscription = new Subscription();
  initIngresoEgresoListenerSubscription: Subscription = new Subscription();

  constructor(private afDB: AngularFirestore,
              private authService: AuthService,
              private store: Store<AppState>) { }

  createIngresoEgreso(ingresoEgreso: IngresoEgreso) {
    const user = this.authService.getUser();
    return this.afDB.doc(`${user.uid}/ingresos-egresos`).collection('items').add({ ... ingresoEgreso });
  }

  deleteIngresoEgreso(uid: string) {
    const user = this.authService.getUser();
    return this.afDB.doc(`${user.uid}/ingresos-egresos/items/${uid}`).delete();
  }

  initIngresoEgresoListener() {
    this.initIngresoEgresoListenerSubscription = this.store.select('auth')
    .pipe(filter(auth => auth.user != null)) // La ejecucion continua si auth.user ha sido asignada
    .subscribe(
      auth => {
        this.ingresoEgresoItems(auth.user.uid);
      }
    );
  }

  closeSubscriptions() {
    this.ingresoEgresoItemsSubscription.unsubscribe();
    this.initIngresoEgresoListenerSubscription.unsubscribe();
    this.store.dispatch( new UnsetItemsAction() );
  }


  private ingresoEgresoItems(uid: string) {
    this.ingresoEgresoItemsSubscription = this.afDB.collection(`${ uid }/ingresos-egresos/items`)
    .snapshotChanges()
    .pipe(
      map( docData => {
        return docData.map( doc => {
          return {
            ...doc.payload.doc.data(),
            uid: doc.payload.doc.id
          };
        } );
      } )
    )
    .subscribe( (dataCollection: IngresoEgreso[]) => {
      console.log(dataCollection);
      this.store.dispatch(new SetItemsAction(dataCollection));
    }
    );
  }
}
