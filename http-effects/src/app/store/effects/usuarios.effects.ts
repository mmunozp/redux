import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import * as usuariosActions from '../actions/usuarios.actions';
import { UsuarioService } from '../../services/usuario.service';
import { CargarUsuariosSuccess } from '../actions/usuarios.actions';
import { of } from 'rxjs';
// El objetivo de los efectos es de escuchar acciones que son mandadas al store

@Injectable()
export class UsuariosEffects {

    constructor(
        private actions$: Actions, // El signo dolar se usa convencionalmente para indicar que es un observable
        public usuarioService: UsuarioService
    ) {}

    @Effect()
    CargarUsuarios$ = this.actions$.pipe(
        ofType(usuariosActions.CARGAR_USUARIOS),
        switchMap( () => {
            return this.usuarioService.getUsers()
                .pipe(
                    map( users => new CargarUsuariosSuccess( users ) ),
                    catchError( error => of(new usuariosActions.CargarUsuariosFail(error)) ) // of convierte en observable
                );
        } )
    );

}

