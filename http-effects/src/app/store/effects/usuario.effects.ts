import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import * as usuarioActions from '../actions/usuario.actions';
import { UsuarioService } from '../../services/usuario.service';
import { CargarUsuarioSuccess, CargarUsuario } from '../actions/usuario.actions';
import { of } from 'rxjs';
// El objetivo de los efectos es de escuchar acciones que son mandadas al store

@Injectable()
export class UsuarioEffects {

    constructor(
        private actions$: Actions, // El signo dolar se usa convencionalmente para indicar que es un observable
        public usuarioService: UsuarioService
    ) {}

    @Effect()
    CargarUsuario$ = this.actions$.pipe(
        ofType(usuarioActions.CARGAR_USUARIO),
        switchMap( (accion: CargarUsuario) => {
            return this.usuarioService.getUserById(accion.id)
                .pipe(
                    map( users => new CargarUsuarioSuccess( users ) ),
                    catchError( error => of(new usuarioActions.CargarUsuarioFail(error)) ) // of convierte en observable
                );
        } )
    );

}

