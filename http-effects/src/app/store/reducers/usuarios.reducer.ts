import { Usuario } from '../../models/usuario.models';
import * as fromUsuarios from '../actions';

export interface UsuariosState {
    users: Usuario[];
    loaded: boolean;
    loading: boolean;
    error: any;
}

const initState: UsuariosState = {
    users: [],
    loaded: false,
    loading: false,
    error: false
};

export function usuariosReducer( state = initState, action: fromUsuarios.usuariosAcciones): UsuariosState {

    switch (action.type) {
        case fromUsuarios.CARGAR_USUARIOS:
            return {
                ...state,
                loading: true,
                error: null
            };

        case fromUsuarios.CARGAR_USUARIOS_FAIL:
            return {
                ...state,
                loaded: false,
                loading: false,
                error: {
                    status: action.payload.status,
                    url: action.payload.url,
                    message: action.payload.message
                }
            };

        case fromUsuarios.CARGAR_USUARIOS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                users: [ ...action.usuarios ]
            };
        default:
            return state;
    }

}
