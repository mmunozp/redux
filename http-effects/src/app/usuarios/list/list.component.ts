import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../models/usuario.models';
import { AppState } from 'src/app/store/app.reducer';
import { Store } from '@ngrx/store';
import { CargarUsuarios } from '../../store/actions/usuarios.actions';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styles: []
})
export class ListComponent implements OnInit {

  usuarios: Usuario[] = [];
  loading: boolean;
  error: any;

  constructor( private store: Store<AppState> ) { }

  ngOnInit() {
    this.store.select('usuarios').subscribe( data => {
      this.usuarios = data.users;
      this.loading = data.loading;
      this.error = data.error;
    } );

    this.store.dispatch( new CargarUsuarios());
  }

}
